Preload = (game) ->

Preload.prototype =
  preload: ->
    @game.load.image 'tile', 'assets/tile.png'
    @game.load.image 'player-right', 'assets/player2.png'
    @game.load.image 'player-left', 'assets/player4.png'
  create: ->
    @game.state.start 'Main'
