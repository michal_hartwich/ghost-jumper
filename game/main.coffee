Main = (game) ->

Main.prototype =
  create: ->
    @spacing = 300
    @score = 0
    @gameSpeed = 0
    @gameStarted = false
    @timerValue = 2500
    @tileWidth = @game.cache.getImage('tile').width
    @tileHeight = @game.cache.getImage('tile').height
    @game.stage.backgroundColor = '479cde'
    @game.physics.startSystem Phaser.Physics.ARCADE
    @platforms = @game.add.group()
    @platforms.enableBody = true
    @platforms.createMultiple 250, 'tile'
    @initPlatforms()
    @createPlayer()
    @createScore()
    @timer = game.time.events.loop(@timerValue, @addPlatform, @)
    @cursors = @game.input.keyboard.createCursorKeys()
    @spacebar = @game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
    @game.input.keyboard.addKeyCapture [ Phaser.Keyboard.SPACEBAR ]

  update: ->
    me = this
    @game.physics.arcade.collide @player, @platforms
    if @cursors.up.isDown and @player.body.wasTouching.down
      @player.body.velocity.y = -1000
    if @cursors.left.isDown
      @player.body.velocity.x += -20
      @player.loadTexture 'player-left'
    if @cursors.right.isDown
      @player.body.velocity.x += 20
      @player.loadTexture 'player-right'
    if @spacebar.isDown and @player.body.wasTouching.down
      @player.body.velocity.y = -1200
      if !@gameStarted
        @gameStarted = true
        @gameSpeed = 100
        @platforms.forEachExists (tile) =>
          tile.body.velocity.y = @gameSpeed
    if @player.body.position.y >= @game.world.height - (@player.body.height)
      @gameOver()

  gameOver: ->
    @game.state.start 'Main'

  addTile: (x, y) ->
    tile = @platforms.getFirstDead()
    tile.reset x, y
    tile.body.velocity.y = @gameSpeed
    tile.body.immovable = true
    tile.checkWorldBounds = true
    tile.outOfBoundsKill = true

  addPlatform: (y) ->
    if typeof y == 'undefined'
      y = -@tileHeight
      @incrementScore()
    tilesNeeded = Math.ceil(@game.world.width / @tileWidth)
    hole = Math.floor(Math.random() * (tilesNeeded - 3)) + 1
    i = 0
    while i < tilesNeeded
      if i < hole or i > hole + 2
        @addTile i * @tileWidth, y
      i++

  initPlatforms: ->
    bottom = @game.world.height - (@tileHeight)
    top = @tileHeight
    y = bottom
    while y > top - (@tileHeight)
      @addPlatform y
      y = y - (@spacing)

  createPlayer: ->
    @player = @game.add.sprite(@game.world.centerX, @game.world.centerY, 'player-right')
    @player.anchor.setTo 0.5, 1.0
    @game.physics.arcade.enable @player
    @player.body.gravity.y = 2000
    @player.body.collideWorldBounds = true
    @player.body.bounce.y = 0.1

  createScore: ->
    scoreFont = '100px Arial'
    @scoreLabel = @game.add.text(@game.world.centerX, 100, '0',
      font: scoreFont
      fill: '#fff')
    @scoreLabel.anchor.setTo 0.5, 0.5
    @scoreLabel.align = 'center'

  incrementScore: ->
    me = this
    if @gameStarted
      @score += 1
    @scoreLabel.text = @score
    if @score == 20
      @gameSpeed = 150
      @timerValue = 2000
      @platforms.forEachExists (tile) =>
        tile.body.velocity.y = @gameSpeed
        return
    else if @score == 40
      @gameSpeed = 200
      @timerValue = 1500
      @platforms.forEachExists (tile) =>
        tile.body.velocity.y = @gameSpeed
