# Ghost jumper game #

To play this game on your local you have to run following commands:
```
$ git clone git@bitbucket.org:michal_hartwich/ghost-jumper.git
$ cd ghost-jumper
$ coffee -c -b game
$ haml index.haml
```
You can find working example [here](http://michalhartwich.com/ghost_game)